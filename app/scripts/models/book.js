/*global define*/

define([
  'underscore',
  'backbone'
], function (_, Backbone) {
  'use strict';

  var BookModel = Backbone.Model.extend({
    url: function(id) {
      id = id || this.id;
      return id ? '/data/book_' + id + '.json' : '/data/books.json';
    },

    parse: function(response)  {
      return response;
    }
  });

  return BookModel;
});
