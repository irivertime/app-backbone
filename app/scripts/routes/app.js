/*global define*/

define([
  'jquery',
  'backbone',
  'collections/book',
  'models/book',
  'views/book',
  'views/app',
  'views/book_show'
], function ($, Backbone, BookCollection, BookModel, BookView, AppView, BookShowView) {
  'use strict';

  var AppRouter = Backbone.Router.extend({

    initialize: function() {
      this.BookList = new BookCollection();
      new AppView({ collection: this.BookList });

      this.BookModel = new BookModel();
      new BookShowView({ model: this.BookModel });
    },

    routes: {
      '':           'indexBook',
      'books':      'indexBook',
      'books/:id':  'showBook'
    },

    indexBook: function() {
      this.BookList.fetch({ reset: true });
    },

    showBook: function(id) {
      this.BookModel.fetch({ url: this.BookModel.url(id) });
    }

  });

  return AppRouter;
});
