/*global require*/
'use strict';

require.config({
  shim: {
    'backbone': {
      'deps': [ 'underscore', 'jquery' ],
      'exports': 'Backbone'
    },
    'underscore': {
      'exports': '_'
    }
  },
  paths: {
    jquery: '../bower_components/jquery/dist/jquery',
    underscore: '../bower_components/underscore/underscore',
    backbone: '../bower_components/backbone/backbone'
  }
});

require([
  'backbone',
  'routes/app'
], function (Backbone, AppRouter) {
  new AppRouter();
  Backbone.history.start();
});
