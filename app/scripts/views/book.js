/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'templates'
], function ($, _, Backbone, JST) {
  'use strict';

  var BookView = Backbone.View.extend({
    template: JST['app/scripts/templates/book_item.ejs'],

    tagName: 'li',

    className: 'media',

    events: {
      'click': 'showBook'
    },

    initialize: function () {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'remove', this.remove);
    },

    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },

    showBook: function (e) {
      e.preventDefault();
      Backbone.history.navigate('books/' + this.model.id, true);
    }
  });

  return BookView;
});
