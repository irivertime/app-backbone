/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'templates'
], function ($, _, Backbone, JST) {
  'use strict';

  var BookShowView = Backbone.View.extend({
    el: '#app',

    template: JST['app/scripts/templates/book_show.ejs'],

    initialize: function () {
      this.listenTo(this.model, 'sync', this.render);
    },

    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  return BookShowView;
});
