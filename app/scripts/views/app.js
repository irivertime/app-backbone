/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
  'views/book'
], function ($, _, Backbone, JST, BookView) {
  'use strict';

  var AppView = Backbone.View.extend({
    el: '#app',

    template: JST['app/scripts/templates/app.ejs'],

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.removePreviousViews);
      this.listenTo(this.collection, 'reset', this.render);
    },

    render: function () {
      this.$el.html(this.template());
      this.collection.each(this.addOne, this);
      return this;
    },

    addOne: function (book) {
      var view = new BookView({model: book});
      this.$('.books').append(view.render().el);
    },

    removePreviousViews: function (col, opts) {
      _.each(opts.previousModels, function(model){
        model.trigger('remove');
      });
    }
  });

  return AppView;
});
