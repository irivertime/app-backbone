/*global define*/

define([
  'underscore',
  'backbone',
  'models/book'
], function (_, Backbone, BookModel) {
  'use strict';

  var BookCollection = Backbone.Collection.extend({
    model: BookModel,
    url: '/data/books.json'
  });

  return BookCollection;
});
